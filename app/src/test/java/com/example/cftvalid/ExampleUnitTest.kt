package com.example.cftvalid

import com.example.cftvalid.models.Validator
import org.junit.Test

import org.junit.Assert.*


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun validate_Name_correct() {
        assertEquals(true, Validator.validateText("Vanya").first)
    }
    @Test
    fun validate_Name_incorrect() {
        assertEquals(false, Validator.validateText("Vanya;;;").first)
    }
    @Test
    fun validate_serName_correct() {
        assertEquals(true, Validator.validateText("Popov").first)
        assertEquals(true, Validator.validateText("Petr-ggg").first)
    }
    @Test
    fun validate_serName_incorrect() {
        assertEquals(false, Validator.validateText("popov;;;").first)
        assertEquals(false, Validator.validateText("").first)
        assertEquals(false, Validator.validateText("3232").first)
    }
    @Test
    fun validate_Date_correct() {
        assertEquals(true, Validator.validateDate("12/12/2000").first)
        assertEquals(true, Validator.validateDate("31/06/1989").first)
    }
    @Test
    fun validate_Date_incorrect() {
        assertEquals(false, Validator.validateDate("60/12/2000").first)
        assertEquals(false, Validator.validateDate("").first)
        assertEquals(false, Validator.validateDate("3232").first)
        assertEquals(false, Validator.validateDate("////").first)
    }

    @Test
    fun validate_Pass_correct() {
        assertEquals(true, Validator.validatePass("QAADD453gfgf").first)
        assertEquals(true, Validator.validatePass("dfsdf454GGG").first)
    }
    @Test
    fun validate_Pass_incorrect() {
        assertEquals(false, Validator.validatePass("dfgfdgfdgfd").first)
        assertEquals(false, Validator.validatePass("4543543534").first)
        assertEquals(false, Validator.validatePass("FDFSDGSDGSFG").first)
        assertEquals(false, Validator.validatePass("fsdfdsSDGSG").first)
        assertEquals(false, Validator.validatePass("fdgfg5t3453").first)
        assertEquals(false, Validator.validatePass("").first)
        assertEquals(false, Validator.validatePass("s1Q").first)
    }
    @Test
    fun validate_ReEnterPass_correct() {
        assertEquals(true, Validator.validateReEnterPass(Pair("QAADD453gfgf","QAADD453gfgf")).first)
        assertEquals(true, Validator.validateReEnterPass(Pair("dfsdf454GGG","dfsdf454GGG")).first)
    }
    @Test
    fun validate_ReEnterPass_incorrect() {
        assertEquals(false, Validator.validateReEnterPass(Pair("dfsdf454GGG","dfsdgfgdf454GGG")).first)
        assertEquals(false, Validator.validateReEnterPass(Pair("dfsd4GGG","dfsdgfgdf454GGG")).first)
        assertEquals(false, Validator.validateReEnterPass(Pair("dfsd4GGG","")).first)
    }
    @Test
    fun validate_Spinner_correct() {
        assertEquals(true, Validator.validateSpinner(4).first)
        assertEquals(true, Validator.validateSpinner(6).first)
    }
    @Test
    fun validate_Spinner_incorrect() {
        assertEquals(false, Validator.validateSpinner(0).first)
    }
}
