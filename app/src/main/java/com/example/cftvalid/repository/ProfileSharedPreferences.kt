package com.example.cftvalid.repository

import android.content.Context
import android.content.SharedPreferences

class ProfileSharedPreferences {
    companion object {
        fun getSharedPreferences(context: Context, Name: String, Mode: Int): SharedPreferences {
            return context.getSharedPreferences(Name, Mode)
        }
    }
}