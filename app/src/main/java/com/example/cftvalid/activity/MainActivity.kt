package com.example.cftvalid.activity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.cftvalid.R
import com.example.cftvalid.repository.ProfileSharedPreferences




class MainActivity : AppCompatActivity() {

    @BindView(R.id.ProfileInfo)
    lateinit var ProfileInfo: TextView

    @OnClick(R.id.CheckedBtn)
    fun checkProfile() =
        if (loadProfile()) ProfileInfo.text =
            getProfile()
        else
            startActivity(Intent(this, SingupActivity::class.java))

    private fun getProfile(): CharSequence? {
        val data =
            ProfileSharedPreferences.getSharedPreferences(this, "Profile", Context.MODE_PRIVATE)
        return "Name: " + data.getString("Name", "") +
                "\nSurname: " + data.getString("Surname", "") +
                "\nSex: " + data.getString("Sex", "") +
                "\nDate: " + data.getString("Date", "")
    }

    private fun loadProfile(): Boolean {
        val data =
            ProfileSharedPreferences.getSharedPreferences(this, "Profile", Context.MODE_PRIVATE)
        return when {
            data.getString("Name", "") == "" -> false
            data.getString("Surname", "") == "" -> false
            data.getString("Sex", "") == "" -> false
            data.getString("Date", "") == "" -> false
            data.getString("Pass", "") == "" -> false
            else -> true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        ProfileInfo.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                ProfileSharedPreferences
                    .getSharedPreferences(this@MainActivity, "Profile", Context.MODE_PRIVATE)
                    .edit().clear().apply()
                ProfileInfo.text = resources.getString(R.string.please_click_button)
                Toast.makeText(this@MainActivity, R.string.delete_profile, Toast.LENGTH_LONG).show()
                return true
            }
        })
    }
}
