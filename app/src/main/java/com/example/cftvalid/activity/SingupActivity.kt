package com.example.cftvalid.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.example.cftvalid.R
import com.example.cftvalid.repository.ProfileSharedPreferences
import com.example.cftvalid.models.Validator
import com.google.android.material.textfield.TextInputLayout


class SingupActivity : AppCompatActivity() {

    @BindView(R.id.NameUser)
    lateinit var nameUser: EditText

    @BindView(R.id.SurnameUser)
    lateinit var surnameUser: EditText

    @BindView(R.id.YearUser)
    lateinit var yearUser: EditText

    @BindView(R.id.PassUser)
    lateinit var passUser: EditText

    @BindView(R.id.PassReEnterUser)
    lateinit var passReEnterUser: EditText


    @BindView(R.id.TextInputLayoutNameUser)
    lateinit var TextInputLayoutnameUser: TextInputLayout

    @BindView(R.id.TextInputLayoutSurnameUser)
    lateinit var TextInputLayoutsurnameUser: TextInputLayout

    @BindView(R.id.TextInputLayoutYearUser)
    lateinit var TextInputLayoutyearUser: TextInputLayout

    @BindView(R.id.TextInputLayoutPassUser)
    lateinit var TextInputLayoutpassUser: TextInputLayout

    @BindView(R.id.TextInputLayoutPassReEnterUser)
    lateinit var TextInputLayoutpassReEnterUser: TextInputLayout

    @BindView(R.id.spinnerSex)
    lateinit var spinnerSex: Spinner

    @BindView(R.id.TextInputSpinnerSex)
    lateinit var TextInputSpinnerSex: TextInputLayout

    @OnClick(R.id.SingupBtn)
    fun singup() {
        if (validate()) {
            ProfileSharedPreferences
                .getSharedPreferences(this, "Profile", Context.MODE_PRIVATE)
                .edit()
                .putString("Name", nameUser.text.toString())
                .putString("Surname", surnameUser.text.toString())
                .putString("Sex", spinnerSex.selectedItem.toString())
                .putString("Date", yearUser.text.toString())
                .putString("Pass", passUser.text.toString())
                .apply()
            finish()
        }
    }

    private fun validate(): Boolean {
        errorClear()
        var Answer: Pair<Boolean, String>
        var flag: Boolean = true

        Answer = Validator.validateText(nameUser.text.toString())
        if (!Answer.first) {
            TextInputLayoutnameUser.error = Answer.second
            flag = false
        }

        Answer = Validator.validateText(surnameUser.text.toString())
        if (!Answer.first) {
            TextInputLayoutsurnameUser.error = Answer.second
            flag = false
        }

        Answer = Validator.validateDate(yearUser.text.toString())
        if (!Answer.first) {
            TextInputLayoutyearUser.error = Answer.second
            flag = false
        }

        Answer = Validator.validatePass(passUser.text.toString())
        if (!Answer.first) {
            TextInputLayoutpassUser.error = Answer.second
            flag = false
        }

        Answer = Validator.validateReEnterPass(
            Pair(passUser.text.toString(), passReEnterUser.text.toString())
        )
        if (!Answer.first) {
            TextInputLayoutpassReEnterUser.error = Answer.second
            flag = false
        }

        Answer = Validator.validateSpinner(spinnerSex.selectedItemPosition)
        if (!Answer.first) {
            TextInputSpinnerSex.error = Answer.second
            flag = false
        }

        return flag
    }

    private fun errorClear() {
        TextInputLayoutnameUser.error = ""
        TextInputLayoutsurnameUser.error = ""
        TextInputLayoutyearUser.error = ""
        TextInputLayoutpassUser.error = ""
        TextInputLayoutpassReEnterUser.error = ""
        TextInputSpinnerSex.error = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_singup)
        ButterKnife.bind(this)
        initSpinner()
    }

    private fun initSpinner() {
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.Sex,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item
        )
        spinnerSex.adapter = adapter
    }
}
