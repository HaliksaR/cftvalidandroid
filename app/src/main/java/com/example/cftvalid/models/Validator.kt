package com.example.cftvalid.models

class Validator {
    companion object {
        fun validateText(arg: String): Pair<Boolean, String> {
            return when {
                arg.isEmpty() ->
                    Pair(false, "Field is Empty")
                arg.length > 20 ->
                    Pair(false, "WOW, why so many? I do not believe you")
                arg.contains(".*\\d.*".toRegex()) ->
                    Pair(false, "The numbers, really?")
                arg.contains("[|/~^:,;?!&%$@*+]".toRegex()) ->
                    Pair(false, "The symbols, really?")
                else ->
                    Pair(true, "Correct")
            }
        }

        fun validateDate(arg: String): Pair<Boolean, String> {
            return when {
                arg.isEmpty() ->
                    Pair(false, "Field is Empty")
                arg.contains("(0[1-9]|[12]\\d|3[01])/(0[1-9]|1[0-2])/[12]\\d{3}".toRegex()) ->
                    Pair(true, "Correct")
                else ->
                    Pair(false, "Incorrect date")
            }
        }

        fun validatePass(arg: String): Pair<Boolean, String> {
            return when {
                arg.isEmpty() ->
                    Pair(false, "Field is Empty")
                !arg.contains("(?=.*[0-9])".toRegex()) ->
                    Pair(false, "You forgot to write a number, bro")
                !arg.contains("(?=.*[a-z])".toRegex()) ->
                    Pair(false, "You forgot to write a lowercase letter, bro")
                !arg.contains("(?=.*[A-Z])".toRegex()) ->
                    Pair(false, "You forgot to write an uppercase letter, bro")
                !arg.contains("(?=\\S+$)".toRegex()) ->
                    Pair(false, "You put a space somewhere, bro")
                !arg.contains(".{8,}".toRegex()) ->
                    Pair(false, "What's so short?")
                else -> return Pair(true, "Correct")
            }
        }

        fun validateReEnterPass(arg: Pair<String, String>): Pair<Boolean, String> {
            return when {
                arg.first == arg.second ->
                    Pair(true, "Correct")
                else ->
                    Pair(false, "Passwords did not match.")
            }
        }

        fun validateSpinner(arg: Int): Pair<Boolean, String> {
            return when (arg) {
                0 -> Pair(false, "Choose gender.")
                else -> Pair(true, "Correct")
            }
        }
    }
}

